package offerservice

import grails.boot.GrailsApp
class Application extends GrailsAutoConfiguration {
    static void main(String[] args) {
        GrailsApp.run(Application, args)
    }
}

import grails.boot.config.GrailsAutoConfiguration
