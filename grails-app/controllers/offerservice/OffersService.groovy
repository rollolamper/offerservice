package offerservice

import grails.async.Promise
import grails.plugins.rest.client.RestBuilder
import org.springframework.beans.factory.annotation.Value

import static grails.async.Promises.task

class OffersService {

    @Value('${urls.loyaltyservice}')
    String loyaltyUrl
    @Value('${urls.discountservice}')
    String discountUrl

    def getResponses(String uuid) {
        if (uuid) loyaltyPromise(uuid) then { result  ->
            discountPromise(result.points).get()
        } get()
        else discountPromise().get()
    }

    Promise loyaltyPromise(String uuid) {
        task {
            RestBuilder rest = new RestBuilder()
            rest.get(loyaltyUrl + (uuid ? "/" + uuid + ".json": "")).json
        }
    }

    Promise discountPromise(points) {
        task {
            RestBuilder rest = new RestBuilder()
            rest.get(discountUrl + ( points ? "?points=" + points : "" )).json
        }
    }
}
