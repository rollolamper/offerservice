package offerservice

class UrlMappings {

    static mappings = {

        get "/offers.json"(controller: 'offer', action:'offers')

        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
