package offerservice


import grails.rest.*
import grails.converters.*

class OfferController {
	static responseFormats = ['json']

    OffersService offersService
	
    def offers() {
        render offersService.getResponses(params.uuid) as JSON
    }
}
