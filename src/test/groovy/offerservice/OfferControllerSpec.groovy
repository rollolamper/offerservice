package offerservice

import grails.plugins.rest.client.RestBuilder
import grails.testing.web.controllers.ControllerUnitTest
import spock.lang.Specification

class OfferControllerSpec extends Specification implements ControllerUnitTest<OfferController> {

    RestBuilder rest

    def setup() {
        rest = new RestBuilder()
    }

    void "two of the happiest paths in the world"() {

        expect: "resounding success"
        rest.get(url).json.collect().size() == discountCount

        where:
        url                                                  | discountCount
        "http://localhost:8080/offers.json?uuid=abracadabra" | 4
        "http://localhost:8080/offers.json"                  | 10

    }
}